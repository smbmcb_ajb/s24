let getCube = 2**3;
console.log(`The cube of 2 is ${getCube}`);

let address = [258, "Washington Ave NW", "California 90011"];
let [add_num, add_street, add_state] = address;

console.log(`I live at ${add_num} ${add_street}, ${add_state}`);

let animal = 
{
	name: "Lolong",
	type: "saltwater crocodile",
	weight: 1075,
	measurement: "20 ft 3 in"
}

let {name, type, weight, measurement} = animal;
console.log(`${name} was a ${type}. He weighed at ${weight} kgs with a mesurement of ${measurement}.`);

let numbers = [1, 2, 3, 4, 5];

numbers.forEach((number)=> console.log(number));

let reduceNumber = numbers.reduce((sum,number)=>{return sum + number});
console.log(reduceNumber);